//Tentando criar uma classe para instanciar e preencher com o json
//da api do facebook

export class feed{
    feed:
    {
        data:
        {
            comments: 
            {
                data:
                {
                    message: any,
                    reactions: 
                    {
                        id: any,
                        name: any,
                        type: any
                    }
                    id: any;
                }
                paging:
                {
                    cursors:
                    {
                        before: any,
                        after: any
                    }
                }    
            }
            created_time: any,
            id: any,
            message: any
        }
        paging:
        {
            previous: any,
            next: any
        }     
    }
    name: any
    id: any
}